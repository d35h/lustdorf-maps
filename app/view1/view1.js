'use strict';

angular.module('myApp.view1', ['ngRoute', 'uiGmapgoogle-maps'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/view1', {
            templateUrl: 'view1/view1.html',
            controller: "View1Ctrl as vm"
        });
    }])

    .controller('View1Ctrl', ['$scope', function ($scope) {
        var vm = this;    
        
        vm.content = 'You clicked on the marker';
       
	    vm.windowOptions = {
	            visible: false
	        };
	    
        vm.map = {
            center: {
                latitude: 46.3264215,
                longitude: 30.6752639
            },
            zoom: 20,
          
        };
        
        vm.marker = {
        		id: 1,
	        	coords:	{
	        		 latitude: 46.326283,
	                 longitude: 30.675273
	        	},
        		options: {
        			title: 'A text to display',
        		},
        		events: {
        			mouseover: function(gMarker, eventName, model, latLngArgs) { console.log('it works'); },
        			click: function() {
        	        	console.log("Clicked!");
        				vm.windowOptions.visible = !vm.windowOptions.visible; 
        	        }        		
        		}
        };
        
        google.maps.event.addListener(vm.marker, "mouseover", function() {
        	console.log('works');
        });
        
        vm.polygons = [
                           {
                               id: 1,
                               path: [
                                   {
                                       latitude: 46.326283,
                                       longitude: 30.675273
                                   },
                                   {
                                       latitude: 46.326262,
                                       longitude: 30.675267
                                   },
                                   {
                                       latitude: 46.326272,
                                       longitude: 30.675216
                                   },
                                   {
                                       latitude: 46.326318,
                                       longitude: 30.675227
                                   }
                               ],
                               stroke: {
                                   color: '#6060FB',
                                   weight: 1
                               },
                               editable: true,
                               draggable: false,
                               geodesic: false,
                               visible: true,
                               fill: {
                                   color: 'silver',
                                   opacity: 0.8
                               }
                           }
                       ];
           

    }]);